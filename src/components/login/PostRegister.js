import React from "react";
import "./PostRegister.scss";
import { connect } from "react-redux";
import EducatorRegister from "./register/EducatorRegister";
import IndividualRegister from "./register/IndividualRegister";
import CorporateRegister from "./register/CorporateRegister";

const PostRegister = ({ user }) => {
  return (
    <div className="col-md-12">
      <div className="row pb-4">
        <div className="col-md-6 mx-auto">
          <h4 className="text-center h2 pt-2 pb-2">
            Welcome to UDEN, {user.name} !
          </h4>
          <p className="text-center">
            Congratulations! You have registered to a platform of great
            opportunities. At UDEN we help you grow in your endavours. Please
            answer some of the questions about yourself to help us know you
            better.
          </p>
        </div>
      </div>
      {user.changeType === "Indvidual" ? (
        <IndividualRegister />
      ) : user.changeType === "Educator" ? (
        <EducatorRegister />
      ) : (
        <CorporateRegister />
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(PostRegister);
