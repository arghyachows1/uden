import React from "react";
import "./Footer.scss";
import udenlogo from "../../../assets/uden-logo.png";

const Footer = () => {
  return (
    <div className="container-fluid mt-4">
      <div className="row">
        <div className="col-md-4">
          <img src={udenlogo} alt="udenlogo" className="uden-logo" />
        </div>
        <div className="col-md-2">
          <ul class="list-group list-group-flush">
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                How it works
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Programs
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Security
              </a>
            </li>
          </ul>
        </div>
        <div className="col-md-2">
          <ul class="list-group list-group-flush">
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Resources
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Jobs
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Partners
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Team
              </a>
            </li>
          </ul>
        </div>
        <div className="col-md-2">
          <ul class="list-group list-group-flush">
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                About
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Blog
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Careers
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Support
              </a>
            </li>
          </ul>
        </div>
        <div className="col-md-2">
          <ul class="list-group list-group-flush">
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Help
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Community Forum
              </a>
            </li>
            <li class="list-group-item border-0">
              <a href="/" className="text-muted">
                Learning Lab
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

Footer.propTypes = {};

export default Footer;
