import React from "react";
import udenlogo from "../../../assets/uden-logo.png";
import "./Header.scss";
const Header = () => (
  <div className="container-fluid">
    <div className="row">
      <div className="col-md-4">
        <img src={udenlogo} alt="udenlogo" className="uden-logo" />
      </div>
      <div className="col-md-1"></div>
      <div className="col-md-2 p-4">
        <a href="/" className="text-warning">
          How it works
        </a>
      </div>
      <div className="col-md-2 p-4">
        <a href="/" className="text-warning">
          Programs
        </a>
      </div>
      <div className="col-md-2 p-4">
        <a href="/" className="text-warning">
          Resources
        </a>
      </div>
      <div className="col-md-1 p-4">
        <a href="/" className="text-warning">
          Pricing
        </a>
      </div>
    </div>
  </div>
);

export default Header;
