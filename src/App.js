import React from "react";
import "./App.scss";
import Layout from "./components/common/layout/Layout";

const App = () => (
  <div className="app">
    <Layout />
  </div>
);

export default App;
